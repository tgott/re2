# RE2

Vítejte v ukázkovém skriptu neuronových sítí z předmětu RE2!

Tento skript demonstruje základní funkce neuronových sítí a ukazuje, jak použít knihovnu Keras (TensorFlow) pro vytvoření, trénování a testování jednoduchých neuronových sítí. Skript obsahuje několik příkladů, které pokrývají základní témata - MNIST convnet, RNN Example - Kafka text, Timeseries anomaly detection using an Autoencoder, miniGPT Example.

Author: Tomas GOTTHANS (gotthans@vutbr.cz)